# TriviaQuiz - Developed by Maulik

A React Native Application

## Install

Run `npm install` to install dependencies

## Run

### Android

Run `react-native run-android`

### IOS

Run `react-native run-ios`


### Steps to check on mobile application

download app from below link

https://drive.google.com/open?id=1FqbcgGwWO9ZoHqiYgrYzy6BFM5Sjodzv

install the apk and open
