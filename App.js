import React from 'react';
import {Router, Scene, Stack} from "react-native-router-flux";
import Home from './src/screens/home/Component';
import Quiz from './src/screens/quiz/Component';
import Result from './src/screens/result/Component';


const App = () => (
  <Router>
    <Stack key="root" >
      <Scene key="home" component={Home} hideNavBar={true} type={'reset'}/>
      <Scene key="quiz" component={Quiz} hideNavBar={true}/>
      <Scene key="result" component={Result} hideNavBar={true}/>
    </Stack>
  </Router>
);

export default App;
