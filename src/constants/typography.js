import { StyleSheet } from 'react-native';
import { colors } from './colors';

export const textStyles = StyleSheet.create({
  bodyTextCenter: {
    fontSize: 16,
    fontWeight: "300",
    fontStyle: "normal",
    letterSpacing: 0.42,
    textAlign: "center",
    color: colors.black,
  },
  buttonText: {
    fontSize: 16,
    fontWeight: "500",
    fontStyle: "normal",
    letterSpacing: 0.25,
    textAlign: "center",
    color: colors.white,
  },
});
