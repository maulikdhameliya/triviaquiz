import {Dimensions, StyleSheet} from 'react-native';
const windowWidth = Dimensions.get('window').width;

const styles = StyleSheet.create({
  button: {
    width: windowWidth-100,
    minWidth: 226,
    height: 48,
    borderRadius: 22,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default styles;
