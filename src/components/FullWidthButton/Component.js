import React, {Component} from 'react';
import {TouchableOpacity, Text, View} from 'react-native';
import PropTypes from 'prop-types';

import {textStyles} from '../../constants/typography';
import styles from './Style';
import {colors} from "../../constants/colors";

class FullWidthButton extends Component {
  render() {
    const {props} = this;
    const backgroundColor = props.disabled ? colors.mediumPink : colors.apple;

    return (
      <TouchableOpacity
        style={styles.button}
        onPress={props.onPress}
      >
        <View
          style={[styles.button,{backgroundColor:backgroundColor}]}
        >
          <Text style={textStyles.buttonText}>{props.buttonText.toUpperCase()}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

FullWidthButton.propTypes = {
  buttonText: PropTypes.string.isRequired,
  onPress: PropTypes.func,
};

export default FullWidthButton;
