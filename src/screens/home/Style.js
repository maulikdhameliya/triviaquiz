import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    backgroundColor:'white',
    alignItems: 'center',
    justifyContent: 'center'
  },
});

export default styles;
