import React, {Component} from 'react';
import styles from './Style';
import {
  View,
} from 'react-native';
import {Actions} from "react-native-router-flux";
import FullWidthButton from "../../components/FullWidthButton/Component";

class Home extends Component {
  constructor(props) {
    super(props);
  }

  handleStartQuiz = () => {
    Actions.quiz();
  };

  renderStartButton = () => {
    return (
      <FullWidthButton
        onPress={() => {
          this.handleStartQuiz()
        }}
        buttonText="Start Quiz"
      />
    );
  };

  render() {
    return (
      <View style={styles.container}>
        {this.renderStartButton()}
      </View>
    );
  }
}

Home.propTypes = {};

export default Home;
