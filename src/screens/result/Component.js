import React, {Component} from 'react';
import styles from './Style';

import {
  View,
  Text, BackHandler
} from 'react-native';
import FullWidthButton from "../../components/FullWidthButton/Component";
import {Actions} from "react-native-router-flux";
import {textStyles} from "../../constants/typography";

class Result extends Component {
  constructor(props) {
    super(props);
  }

  handlePlayAgain = () => {
    Actions.quiz();
  };


  renderPlayAgainButton = () => {
    return (
      <FullWidthButton
        onPress={() => {
          this.handlePlayAgain()
        }}
        buttonText="Play Again"
      />
    );
  };

  render() {
    return (
      <View style={styles.container}>
        <Text style={[textStyles.bodyTextCenter, styles.questionText]}>Score: {this.props.score} / 10</Text>
        <Text style={[textStyles.bodyTextCenter, styles.questionText]}>Time taken: Less
          then {this.props.timeTaken + 1} minutes</Text>
        <View style={{marginVertical: 20}}>
          {this.renderPlayAgainButton()}
        </View>
      </View>
    );
  }
}

Result.propTypes = {};

export default Result;
