import {StyleSheet} from 'react-native';
import {colors} from "../../constants/colors";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    backgroundColor: colors.white,
    justifyContent: 'center',
    alignItems: 'center'
  }
});

export default styles;
