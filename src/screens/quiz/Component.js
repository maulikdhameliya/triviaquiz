import React, {Component} from 'react';
import styles from './Style';

let decode = require('unescape');

import {
  View,
  FlatList,
  Text,
  TouchableWithoutFeedback
} from 'react-native';
import {getQuestions} from "../../api/Api";
import {textStyles} from "../../constants/typography";
import FullWidthButton from "../../components/FullWidthButton/Component";
import {Actions} from "react-native-router-flux";

import moment from "moment";

let startDate,endDate;

class Quiz extends Component {
  constructor(props) {
    super(props);
    this.state = {
      questions: [],
      selectedAnswers: [],
    }
  }

  componentDidMount = async () => {
    let quizData = await getQuestions();
    this.setData(quizData);
  };

  setData = (quizData) => {
    if (quizData && quizData.response_code === 0 && quizData.results) {
      if (quizData.results.length > 0) {
        this.setState({questions: quizData.results});
        startDate=new Date();
      }
    }
  };


  renderQuestionDescription = (index, question) => {
    return (
      <View
        style={styles.descriptionContainer}
      >
        <Text
          style={[textStyles.bodyTextCenter, styles.questionText]}>({index + 1}/{this.state.questions.length}) {decode(question)}
        </Text>
      </View>
    );
  };

  renderAnswers = (answers, questionIndex) => {
    return answers.map((answer, index) => {
      return this.renderAnswer(
        index,
        answer,
        questionIndex
      );
    });
  };

  handleAnswerSelect = (text, questionIndex) => {
    const selectedAnswers = this.state.selectedAnswers;
    selectedAnswers[questionIndex] = text;
    this.setState({
      selectedAnswers,
    });
  };

  renderAnswer = (index, text, questionIndex) => {
    const isSelectedAnswer = this.state.selectedAnswers[questionIndex] === text;
    return (
      <TouchableWithoutFeedback
        key={text}
        onPress={() => this.handleAnswerSelect(text, questionIndex)}
      >
        <View
          style={styles.answerBorderContainer}
        >
          <View style={styles.answerTextContainer}>
            <Text style={textStyles.inputText}>
              {text}
            </Text>
          </View>
          <View style={styles.answerImageContainer}>
            <View style={styles.outerCircle}>
              {
                isSelectedAnswer &&
                <View style={styles.innerCircle}/>
              }
            </View>
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  };

  renderQuizItem = (item, index) => {
    return <View
      key={item}
      style={styles.questionCardStyle}
    >
      {this.renderQuestionDescription(index, item.question)}
      <View style={styles.answersContainer}>
        {this.renderAnswers([...item.incorrect_answers, item.correct_answer], index)}
      </View>
    </View>
  };

  renderQuiz = (questions) => {
    return <FlatList
      data={questions}
      extraData={this.state}
      renderItem={({item, index}) => {
        return this.renderQuizItem(item, index);
      }}
    />
  };

  handleCompletePress = (selectedAnswers) => {
    endDate = new Date();
    if (this.shouldDisableCompleteButton(selectedAnswers)) {
      alert("Please answer all questions")
    } else {
      let correctAnswers = 0;
      selectedAnswers.map((answer, index) => {
        console.log("answer", selectedAnswers[index]);
        console.log("correctAnswer", this.state.questions[index].correct_answer);
        if (this.state.questions[index].correct_answer === selectedAnswers[index]) {
          correctAnswers++;
        }
      });
      const timeTaken = moment(endDate).diff(startDate, "minutes");
      Actions.result({score: correctAnswers, timeTaken: timeTaken});
    }
  };

  shouldDisableCompleteButton = (selectedAnswers) => {
    if (!selectedAnswers) {
      return true;
    }
    const answered = selectedAnswers.filter(selectedAnswer => selectedAnswer !== null);
    const questions = this.state.questions;
    return !(answered.length > 0 && questions && answered.length === questions.length);
  };

  renderCompleteButton = () => {
    return (
      <FullWidthButton
        onPress={() => {
          this.handleCompletePress(this.state.selectedAnswers)
        }}
        buttonText="Complete"
        disabled={this.shouldDisableCompleteButton(this.state.selectedAnswers)}
      />
    );
  };

  render() {
    return (
      <View style={styles.container}>
        {this.state.questions.length > 0 && this.renderQuiz(this.state.questions)}
        <View style={{paddingTop: 10,}}>
          {this.state.questions.length > 0 && this.renderCompleteButton()}
        </View>
      </View>
    );
  }
}

Quiz.propTypes = {};

export default Quiz;
