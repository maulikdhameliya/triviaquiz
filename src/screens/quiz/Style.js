import {Dimensions, StyleSheet} from 'react-native';
import {colors} from "../../constants/colors";

const windowWidth = Dimensions.get('window').width;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems:'center',
    marginVertical: 10,
  },
  questionCardStyle: {
    backgroundColor: colors.white,
    marginBottom: 10,
    marginHorizontal: 8,
    borderRadius: 8,
    width: windowWidth,
  },
  descriptionContainer: {
    marginTop: 24,
    marginBottom: 24,
    paddingHorizontal: 10,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  questionText: {
    textAlign: 'left',
    fontWeight: '500',
  },
  answerTextContainer: {
    width: '90%',
    paddingTop: 10,
    paddingBottom: 10,
    paddingHorizontal: 10,
  },
  answerImageContainer: {
    width: '10%',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 10,
  },
  outerCircle: {
    width: 20,
    height: 20,
    backgroundColor: colors.paleGrey,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 2,
    borderColor: colors.cornflower,
    borderRadius: 25,
  },
  innerCircle: {
    width: 10,
    height: 10,
    backgroundColor: colors.cornflower,
    borderRadius: 25,
  },
  answersContainer: {
    width: windowWidth - 30,
  },
  answerBorderContainer: {
    width: windowWidth,
    minHeight: 60,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingTop: 10,
    paddingRight: 16,
    borderBottomColor: 'rgba(0,0,0,0.15)',
    borderBottomWidth: 1,
  },
});

export default styles;
