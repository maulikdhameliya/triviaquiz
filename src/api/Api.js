import axios from "axios/index";
import {makeGetRequest} from "./RequestExecutor";
import {GET_QUESTIONS} from "./Url";

const API_LOG = true;

axios.interceptors.request.use(request => {
  if (API_LOG)
    console.log('====>request<====', request);
  return request
});

export async function getQuestions() {
  let responseData;
  responseData = makeGetRequest(GET_QUESTIONS);
  return responseData;
}
