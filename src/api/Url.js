export const BASE_URL = 'https://opentdb.com/api.php';

/* GET QUESTIONS */
export const GET_QUESTIONS = BASE_URL + '?amount=10';
