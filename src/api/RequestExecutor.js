import axios from "axios/index";

const API_LOG = true;

const requestType = {
    POST: 'post',
    GET: 'get',
    PUT: 'put',
    DELETE:'delete'
};

axios.interceptors.request.use(request => {
    if (API_LOG)
        console.log('====>request<====', request);
    return request
});

export async function executeRequest(method: string, url: string, headers: any, params: any, data: any) {
    let response = await axios({
        method: method,
        url: url,
        headers: headers,
        params: params,
        data: data,
    })
        .then(async function (response) {
            if (response.data)
                return response.data;
            else
                return response;
        })
        .catch(function (error) {
            if (error.response) {
                if (API_LOG)
                    console.warn('====>Error<====', error.response.data);
                return error.response.data;
            } else if (error.request) {
                if (API_LOG)
                    console.warn('====>Error<====', "No response received");
                return {};
            } else {
                if (API_LOG)
                    console.warn('====>Error<====', "Something went wrong");
                return {};
            }
        });

    if (API_LOG)
        console.log('====>response<====', response);

    return response;
}

export async function makeGetRequest(url) {
  return await executeRequest(requestType.GET, url, null, null, null);
}





